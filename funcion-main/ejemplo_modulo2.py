#!/usr/bin/python
import ejemplo_modulo1

def main():
    print("El siguiente archivo se está ejecutando directamente")
    print("ejemplo_modulo2.py __name__ = %s" %__name__)
    gato = ejemplo_modulo1.Gato("félix")
    gato.maullar()
    gato.saltar()

if __name__ == "__main__":
    main()
