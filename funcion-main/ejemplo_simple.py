#!/usr/bin/python
# Programa de Python para mostrar el uso de la función main()

# Definir la función main()
def main():
    print("Ejecutando la función principal")

# Usar la variable especial __name__ para ejecutar la función main()
if __name__ == "__main__":
    main()
