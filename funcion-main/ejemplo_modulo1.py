#!/usr/bin/python

class Gato(object):
    """Un simple intento de modelar un gato."""

    def __init__(self, nombre):
        """Inicializar los atributos de nombre."""
        self.nombre = nombre

    def maullar(self):
        """Simular un gato maullando en respuesta a un comando."""
        print(self.nombre.title() + " está ahora maullando.")

    def saltar(self):
        """Simular un gato saltando en respuesta a un comando."""
        print(self.nombre.title() + " saltó.")

if __name__ == "__main__":
    print("El siguiente archivo se está ejecutando directamente")
    print("ejemplo_modulo1.py __name__ = %s" %__name__)
else:
    print("El archivo se está importando")
    print("ejemplo_modulo1.py __name__ = %s" %__name__)
